This software calculates the microphysical process rates
needed for the single category ice description.

Author: Remo Dietlicher, 2018
Based on the ideas of Morrison & Milbrandt 2015

---------------------------------------------------------------------------------------------------------------
|                                         Dependencies                                                        |
---------------------------------------------------------------------------------------------------------------

numpy, scipy, matplotlib

---------------------------------------------------------------------------------------------------------------
|                                         Example usecase                                                     |
---------------------------------------------------------------------------------------------------------------

Initialize an empty forcing file (which can for example be edited by the easync GUI):
python create_forcing.py force_one_day.nc 24 3600

Initialize a predefined forcing scenario (the input here produces the mixed-phase cloud shown in 
../Papers/Dietlicher2018a_GMD):
python create_forcing.py specific_scenario.nc 36 3600 --std_atm


---------------------------------------------------------------------------------------------------------------
|                                         Directory contents                                                  |
---------------------------------------------------------------------------------------------------------------

    * create_forcing.py: contains all information needed to set up empty forcing fils for the ECHAM
                         single column model (SCM). So far vertical levels are hard-coded but this could
			 easily be adapted to work with levels from an input-file. The code is structured
			 such that first an empty file is generated and then data is written to the
			 file. There already exist multiple scenarios which can be called with the respective
			 command line flag. New scenarios can be added analogously.
    * generic_atmosphere.py: backbone of the scenario generation. This module takes care of interpolating
                             piecewise linear input data, such as temperature and RH profiles as a function
			     of pressure, but also any other variable in pressure space to the model grid
			     with specified model to pressure level conversion. 
